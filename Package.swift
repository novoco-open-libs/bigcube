// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BigCube",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "BigCube",
            targets: ["BigCube"]
        ),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(
            name: "Flynn",
            url: "https://github.com/novocodev/flynn.git",
            .branch("master")
        ),
        .package(
            name: "BigInt",
            url: "https://github.com/attaswift/BigInt.git",
            from: "5.2.1"
        ),
        .package(
            name: "Quick",
            url: "https://github.com/Quick/Quick",
            from: "4.0.0"
        ),
        .package(
            name: "Nimble",
            url: "https://github.com/Quick/Nimble",
            from: "9.2.0"
        ),
        .package(
            name: "SwiftTasks",
            url: "https://gitlab.com/novoco-open-libs/swift-tasks.git",
            .branch("master")
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "BigCube",
            dependencies: ["Flynn", "BigInt"]
        ),
        .testTarget(
            name: "BigCubeTests",
            dependencies: ["Flynn", "Nimble", "Quick", "BigCube"]
        ),
    ]
)
