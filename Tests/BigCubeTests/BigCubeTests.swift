    import Quick
    import Nimble
    
    @testable import BigCube

    final class BitCubeTests: QuickSpec {
        override func spec() {
            
            func doTestInitialize() {
                it("can't initialize BigCube") {
                    expect(BigCube(4) != nil).to(equal(true))
                }
            }
            
            describe("initialization") {
                doTestInitialize()
            }
        }
    }
